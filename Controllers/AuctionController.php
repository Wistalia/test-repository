<?php
	namespace App\Controllers;

	use App\Core\Controller;
	use App\Models\CategoryModel;
	use App\Models\AuctionModel;
	use App\Models\OfferModel;
	use App\Models\AuctionViewModel;




	class AuctionController extends Controller {
		public function show($id) {

			$auctionModel = new AuctionModel($this->getDatabaseConnection());
			$auction = $auctionModel->getById($id);

			if(!$auction){
				header("Location: {{ BASE }}");
				exit;
			}

			$this->set('auction', $auction);

			$offerModel = new OfferModel($this->getDatabaseConnection());
			$lastOfferPrice = $offerModel->getLastOfferPrice($auction);
			$this->set('lastOfferPrice', $lastOfferPrice);

			$auctionViewModel = new AuctionViewModel($this->getDatabaseConnection());

			$ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
			$userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');

			$auctionViewModel->add([

				'auction_id' =>  $id,
				'ip_address' =>  $ipAddress,
				'user_agent' =>  $userAgent,


			]);
		}
					
		private function normaliseKeywords(string $keywords): string {

				//ovo je postupak normalizacije, koristi se npr za search input
			$keywords = trim($keywords);
			$keywords = preg_replace('/ +/', ' ', $keywords);

			return $keywords;
		}

		public function postSearch() {
			$auctionModel = new \App\Models\AuctionModel($this->getDatabaseConnection());

			$q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);
			
			$keywords = $this->normaliseKeywords($q);

			$auctions = $auctionModel->getAllBySearch($keywords); // ovo $q je sve ono sto je korisnik ukucao u search polje.

			$this->set('auctions', $auctions);
		}

		
	}
