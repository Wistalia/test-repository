<?php
	namespace App\Controllers;

	use App\Core\DatabaseConnection;
	use App\Models\CategoryModel;
	use App\Models\AuctionViewModel;
	use App\Models\AuctionModel;


	use App\Core\Controller;


	class MainController extends Controller
	{

		public function home(){
			$categoryModel = new CategoryModel($this->getDatabaseConnection());
			$categories = $categoryModel->getAll();
			$this->set('categories', $categories);
		}

		public function getRegister() {
			# ...

		}

		public function postRegister() {
			$email = filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
			$fname = filter_input(INPUT_POST, 'reg_fname', FILTER_SANITIZE_STRING);
			$lname = filter_input(INPUT_POST, 'reg_lname', FILTER_SANITIZE_STRING);
			$username = filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
			$password1 = filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
			$password2 = filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

			/* print_r([
				$email	,
				$fname	,
				$lname	,
				$username,
				$password1,
				$password2,
			]); */

			if ($password1 !== $password2) {
				$this->set('message', 'Doslo je do greske: Niste uneli dva puta istu lozinku.');
				return;
			}

			$validatePassword = (new \App\Validators\StringValidator())
														->setMinLength(7)
														->setMaxLength(150)
														->isValid($password1);

			if(! $validatePassword) {
				$this->set('message', 'Doslo je do greske: loznka nije ispravnog formata');
				return;
			}

			$userModel = new \App\Models\UserModel($this->getDatabaseConnection());

			$user = $userModel->getByFieldName('email', $email);
			if ($user) {

				$this->set('message', 'Doslo je do greske, vec postoji korisnik sa tim emailom.');
				return;

			}

			$user = $userModel->getByFieldName('username', $username);
			if ($user) {

				$this->set('message', 'Doslo je do greske, vec postoji korisnik sa tim korisnickim imenom.');
				return;

			}

			$passwordHash = password_hash($password1, PASSWORD_DEFAULT);
			$userId = $userModel->add([
				'username' => $username,
				'password' => $passwordHash,
				'email' => $email,
				'fname' => $fname,
				'lname' => $lname,

			]);

			if(!$userId) {

				$this->set('message', 'Doslo je do greske: Nije bilo uspesno registrovanje naloga.');
			}
			$this->set('message', 'Napravljen je novi nalog! Sada mozete da se prijavite.');
		}

		public function getLogin() {


		}

		public function postLogin() {

			$username = filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
			$password = filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

			$validatePassword = (new \App\Validators\StringValidator())
												->setMinLength(7)
												->setMaxLength(150)
												->isValid($password);

			if (!$validatePassword) {
				$this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata.');
				return;
			}

			$userModel = new \App\Models\UserModel($this->getDatabaseConnection());

			$user = $userModel->getByFieldName('username', $username);
			if(!$user) {
				$this->set('message', 'Doslo je do greske: Ne postoji korisnik sa tim korisnickim imenom.');
				return;
			}

			$passwordHash = $user->password;

			if(password_verify($password, $passwordHash)) {
//ova funkcija sprecava izvrsavanje nekog napdaca da provali sifru ako je potrefio uzername
				sleep(1); //ovako moze program samo da proba jednu sifru u sekundi.
				$this->set('message', 'Doslo je do greske: Lozinka nije ispravna.');
				return;
			}

			$this->getSession()->put('user_id', $user->user_id);
			$this->getSession()->save();

			$this->redirect(\Configuration::BASE . 'user/profile');


		}
	}
