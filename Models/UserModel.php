<?php
	namespace App\Models;


	use App\Core\Model;
	use App\Core\Field;
	use App\Validators\NumberValidator;
	use App\Validators\DateTimeValidator;
	use App\Validators\StringValidator;
	use App\Validators\BitValidator;

	class UserModel extends Model
	{

			protected function getFields(): array {
		return [

			'user_id'		=> new Field((new NumberValidator())->setIntegerLength(11), false),

			'username'    		=> new Field((new StringValidator())->setMaxLength(255)),
			'fname'    		=> new Field((new StringValidator())->setMaxLength(255)),
			'lname'    		=> new Field((new StringValidator())->setMaxLength(255)),
			'email'	   	    => new Field((new StringValidator())->setMaxLength(255)),
			'password'		=> new Field((new StringValidator())->setMaxLength(255)),
			'is_active'	    => new Field(new BitValidator())

		];
	}



		public function getByFname(string $fname){
			return $this->getByFieldName('fname', $fname);

		}
	}
